ARG VERSION=latest

FROM ubuntu:${VERSION}

COPY install.sh /root/
RUN /bin/bash /root/install.sh

USER ubuntu
