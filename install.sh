#!/bin/bash
# Stop on error
set -e
export DEBIAN_FRONTEND=noninteractive

# Install mandatory programs and utilities
apt-get --yes --quiet=2 update
apt-get --yes --quiet=2 install \
	ansible \
	apt-transport-https \
	ca-certificates \
	curl \
	dnsutils \
	git \
	procps \
	rsync \
	wget \
	unzip \
	zip
apt-get clean
rm -rf /var/lib/apt/lists/*

# Ansistrano
ansible-galaxy install ansistrano.deploy ansistrano.rollback
su - ubuntu -c 'ansible-galaxy install ansistrano.deploy ansistrano.rollback'
